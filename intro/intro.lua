local intro = {}

local path = "intro/"

local frameTime = 0.04
local playSoundAt = 0.5

local fadeColors

local palette = {
    {c={0,0,0}, avg=0},
    {c={0,0.14285714285714,0.57142857142857}, avg=0.23809523809524},
    {c={0.42857142857143,0.42857142857143,0.42857142857143}, avg=0.42857142857143},
    {c={0.71428571428571,0.71428571428571,0.71428571428571}, avg=0.71428571428571},
    {c={0.71428571428571,0.85714285714286,1}, avg=0.85714285714286},
    {c={1,1,1}, avg=1},
}

local transitionTypes = {"fade", "lines", "wave"}

local waveShader, shaderCanvas

function intro.load(w, h, scale)
    intro.transitionType = "wave"--transitionTypes[love.math.random(#transitionTypes)]

    intro.width = w
    intro.height = h
    intro.scale = scale

    if intro.transitionType == "wave" then
        waveShader = love.graphics.newShader([[
            extern float intensity;
            extern float timer;
            extern float height;

            vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
            {
                float useY = ceil(texture_coords.y*height)/height;
                vec4 texcolor = Texel(texture, texture_coords + vec2(sin(timer*10+useY*30)*intensity, 0));
                return texcolor * color;
            }
        ]])

        waveShader:send("height", intro.height/intro.scale/2)
        shaderCanvas = love.graphics.newCanvas(intro.width, intro.height)
    end

    intro.stabSound = love.audio.newSource(path .. "stab.ogg", "static")

    intro.dudeImg = love.graphics.newImage(path .. "dude.png")
    intro.dudeImg:setFilter("nearest", "nearest")
    intro.dudeFrames = {}

    for y = 1, 5 do
        for x = 1, y == 5 and 1 or 4 do
            table.insert(intro.dudeFrames, love.graphics.newQuad(1+(x-1)*88, 1+(y-1)*51, 87, 50, 353, 256))
        end
    end

    intro.titleImg = love.graphics.newImage(path .. "title.png")
    intro.titleImg:setFilter("nearest", "nearest")

    intro.timing = {
        {name="emptyWait", d=0.3},
        {name="fadeIn", d=0.3},
        {name="knifeWait", d=0.5},
        {name="animation", d=#intro.dudeFrames*frameTime},
        {name="finishWait", d=1.5},
        {name="fadeOut", d=0.3},
        {name="finished", d=math.huge}
    }

    intro.state = 1
    intro.timer = 0
end

function intro.update(dt)
    intro.timer = intro.timer + dt

    if not intro.soundPlayed and intro.timing[intro.state].name == "animation" and intro.timer >= playSoundAt then
        intro.stabSound:play()
        intro.soundPlayed = true
    end

    while intro.timer > intro.timing[intro.state].d do
        intro.timer = intro.timer - intro.timing[intro.state].d
        intro.state = math.min(#intro.timing, intro.state + 1)
    end
end

function intro.transition(i)
    if intro.transitionType == "fade" then
        intro.setClosestColor(i)

    elseif intro.transitionType == "lines" then
        local lineHeight = 8*intro.scale

        love.graphics.stencil(function()
            love.graphics.push()
            love.graphics.origin()
            for y = 0, intro.height-1, lineHeight do
                love.graphics.rectangle("fill", 0, y, intro.width, math.ceil(lineHeight*i/intro.scale)*intro.scale)
            end
            love.graphics.pop()
        end, "replace", 1)
        love.graphics.setStencilTest("greater", 0)

    elseif intro.transitionType == "wave" then
        waveShader:send("intensity", (1-i)*0.1)
        waveShader:send("timer", intro.timer)
        love.graphics.setCanvas(shaderCanvas)
        love.graphics.clear()
    end
end

function intro.transitionCleanup(i)
    if intro.transitionType == "fade" then
        love.graphics.setColor(1, 1, 1)

    elseif intro.transitionType == "lines" then
        love.graphics.setStencilTest()

    elseif intro.transitionType == "wave" then
        love.graphics.setCanvas()
        love.graphics.setShader(waveShader)
        love.graphics.push()
        love.graphics.origin()
        love.graphics.setColor(1, 1, 1, i)
        print(i)
        love.graphics.draw(shaderCanvas)
        love.graphics.setColor(1, 1, 1)
        love.graphics.pop()
        love.graphics.setShader()
    end
end

function intro.setClosestColor(a)
    -- find closest color
    local closest = 1
    for i, color in ipairs(palette) do
        if math.abs(a-color.avg) < math.abs(a-palette[closest].avg) then
            closest = i
        end
    end

    love.graphics.setColor(palette[closest].c)
end

function intro.draw()
    love.graphics.push()
    love.graphics.translate(intro.width/2, intro.height/2)
    love.graphics.scale(intro.scale*2, intro.scale*2)
    love.graphics.translate(0, -10)

    local name = intro.timing[intro.state].name

    if name == "fadeIn" then
        local i = intro.timer/intro.timing[intro.state].d
        intro.transition(i)
        love.graphics.draw(intro.dudeImg, intro.dudeFrames[1], 0, 0, 0, 1, 1, 43, 25)
        intro.transitionCleanup(i)

    elseif name == "knifeWait" then
        love.graphics.draw(intro.dudeImg, intro.dudeFrames[1], 0, 0, 0, 1, 1, 43, 25)

    elseif name == "animation" then
        local frame = math.min(math.ceil(intro.timer/frameTime), #intro.dudeFrames)
        love.graphics.draw(intro.dudeImg, intro.dudeFrames[frame], 0, 0, 0, 1, 1, 43, 25)

        love.graphics.stencil(function()
            love.graphics.draw(intro.titleImg, 0, 51, 0, 1, 1, 43, 25)
        end, "replace", 1)
        love.graphics.setStencilTest("greater", 0)

        local posStart = 51-intro.titleImg:getHeight()
        local posChange = intro.titleImg:getHeight()

        local moveAfter = 0.53
        local moveOver = 0.05

        local progress = math.max(0, math.min(1, (intro.timer-moveAfter)/moveOver))

        local pos = posChange * math.sin(progress * (math.pi / 2)) + posStart

        love.graphics.draw(intro.titleImg, 0, pos, 0, 1, 1, 43, 25)

        love.graphics.setStencilTest()

    elseif name == "finishWait" then
        love.graphics.draw(intro.dudeImg, intro.dudeFrames[#intro.dudeFrames], 0, 0, 0, 1, 1, 43, 25)
        love.graphics.draw(intro.titleImg, 0, 51, 0, 1, 1, 43, 25)

    elseif name == "fadeOut" then
        local i = 1-intro.timer/intro.timing[intro.state].d
        intro.transition(i)
        love.graphics.draw(intro.dudeImg, intro.dudeFrames[#intro.dudeFrames], 0, 0, 0, 1, 1, 43, 25)
        love.graphics.draw(intro.titleImg, 0, 51, 0, 1, 1, 43, 25)
        intro.transitionCleanup(i)
    end

    love.graphics.pop()
end

return intro
