function love.load()
    frameDebug3 = require "frameDebug3"
    intro = require "intro.intro"
    intro.load(1200, 672, 3)
end

function love.update(dt)
    dt = frameDebug3.update(dt)
    intro.update(dt)
end

function love.draw()
    intro.draw()
end

function love.keypressed(key, scancode)
    if key == "escape" then
        love.event.quit()
    end
end

function VAR()
    return {{key="-", val=0.1}}
end